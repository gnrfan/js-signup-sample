<!DOCTYPE html>
<html>
<head>
<title>Formulario de prueba</title>
<script type="text/javascript" src="jquery-1.7.1.min.js"></script> 
<link rel="stylesheet" href="formulario.css" />
</head>
<body>
<h1>Formulario de prueba</h1>
<form action="" method="POST">
<table border="1">
<tr id="username_container">
<td>Username</td>
<td>:</td>
<td><input type="text" id="username_input" value="" /></td>
<td>
<div class="msg okmsg">El username libre</div>
<div class="msg errormsg">El username esta usado por otro usuario</div>
</td>
</tr>
<tr id="password_container">
<td>Password</td>
<td>:</td>
<td><input type="password" id="password_input" value="" /></td>
</tr>
</table>
<p><input type="submit" value="Enviar" /></p>
</form>
<script type="text/javascript" src="formulario.js"></script> 
</body>
</html>
