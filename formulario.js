(function($, document){

  $(document).ready(function() {
    setup_username_input(); 
  });

  function setup_username_input() {
    var timeout_id;
    var $username_container = $('#username_container');
    var $username_input = $('#username_input');
    function check_username() {
      var value = $username_input.val();
      if (value.length < 2) {
        // ocultamos los mensajes de error
        $username_container.removeClass('okmsg errormsg');
        return ;
      }
      $.getJSON('/js1/check_user.php', {'username': value}, function(data) {
        var display_class;
        if (data['username_exists'] === 1) {
            display_class = 'okmsg';
        } else {
            display_class = 'errormsg';
        }
        // Quitamos las dos clases de error sin quitar otras clases
        $username_container.removeClass('okmsg errormsg');
        $username_container.addClass(display_class);
      });
    }
    $username_input.bind('keyup', function() {
      clearTimeout(timeout_id);
      timeout_id = setTimeout(check_username, 200);
    });
  }

  function setup_password_input() {
    var timeout_id;
    var $username_container = $('#username_container');
    var $username_input = $('#username_input');
    function check_username() {
      var value = $username_input.val();
      if (value.length < 2) {
        // ocultamos los mensajes de error
        $username_container.removeClass('okmsg errormsg');
        return ;
      }
      $.getJSON('', {'username': value}, function(data) {
        var display_class;
        if (data['username_exists'] === 1) {
            display_class = 'okmsg';
        } else {
            display_class = 'errormsg';
        }
        // Quitamos las dos clases de error sin quitar otras clases
        $username_container.removeClass('okmsg errormsg');
        $username_container.addClass(display_class);
      });
    }
    $username_input.bind('keyup', function() {
      clearTimeout(timeout_id);
      timeout_id = setTimeout(check_username, 200);
    });
  }

}(jQuery, document));
