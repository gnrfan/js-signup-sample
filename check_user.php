<?php

$username = '';
if (array_key_exists('username', $_REQUEST)) {
    $username = $_REQUEST['username'];
}

$lottery = rand(1, 100);
$check = ($lottery % 7 == 0);

$result = array();
if ($check) {
  $result['username_exists'] = 1; 
} else {
  $result['username_exists'] = 0; 
}

header("Content-type: application/json");
echo json_encode($result)."\n";

?>
